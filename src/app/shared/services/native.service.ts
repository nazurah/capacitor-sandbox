import { Injectable } from '@angular/core';
import {
  Plugins,
  CameraOptions,
  CameraSource,
  CameraResultType,
} from '@capacitor/core';
import { Platform } from '@ionic/angular';
const { Camera, BarcodeScanner } = Plugins;

declare var window: any;

@Injectable({
  providedIn: 'root',
})
export class NativeService {
  constructor(private platform: Platform) {
    BarcodeScanner.prepare();
  }

  isNative(): boolean {
    return window.cordova ? true : false;
  }

  getPlatform(): 'ios' | 'android' | 'browser' {
    if (this.isNative()) {
      if (this.platform.is('ios')) {
        return 'ios';
      } else if (this.platform.is('android')) {
        return 'android';
      }
    } else {
      return 'browser';
    }
  }

  async getPhoto(options?: CameraOptions) {
    let defaultOptions: CameraOptions = {
      quality: 100,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Camera,
      width: 200,
      height: 200,
      preserveAspectRatio: true,
    };
    return await Camera.getPhoto({
      ...defaultOptions,
      ...options,
    });
  }
}
