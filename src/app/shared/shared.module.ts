import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { SafeHtmlPipe } from './services/pipe/safe-html.pipe';

@NgModule({
  declarations: [SafeHtmlPipe],
  imports: [CommonModule, IonicModule, ReactiveFormsModule],
  exports: [CommonModule, IonicModule, ReactiveFormsModule, SafeHtmlPipe],
})
export class SharedModule {}
