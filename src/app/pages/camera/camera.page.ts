import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CameraOptions, CameraResultType, Plugins } from '@capacitor/core';

const { Camera } = Plugins;

@Component({
  selector: 'app-camera',
  templateUrl: './camera.page.html',
  styleUrls: ['./camera.page.scss'],
})
export class CameraPage implements OnInit {
  form: FormGroup;
  imagePath: string =
    'https://reactnativecode.com/wp-content/uploads/2018/02/Default_Image_Thumbnail.png';

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.form = this.formBuilder.group({
      quality: [
        50,
        Validators.compose([Validators.min(0), Validators.max(100)]),
      ],
      allowEditing: [false],
      resultType: ['uri', Validators.required],
      width: [500, Validators.compose([Validators.min(0)])],
      height: [500, Validators.compose([Validators.min(0)])],
      preserveAspectRatio: [true],
      correctOrientation: [true],
      source: ['CAMERA'],
      direction: ['REAR'],
      presentationStyle: [null],
      promptLabelHeader: [null],
      promptLabelCancel: [null],
      promptLabelPhoto: [null],
      promptLabelPicture: [null],
    });
  }

  async submit() {
    if (this.form.valid) {
      let data = this.form.value;
      console.log(data);

      this.getPhoto(data);
    }
  }

  async getPhoto(options: CameraOptions) {
    const result = await Camera.getPhoto(options);
    const { resultType } = options;
    if (result) {
      console.log(result);
      switch (resultType) {
        case CameraResultType.DataUrl:
          this.imagePath = result.dataUrl;
          break;

        case CameraResultType.Base64:
          this.imagePath = 'data:image/png;base64,' + result.base64String;
          break;

        case CameraResultType.Uri:
          this.imagePath = result.webPath;
          break;
      }
    }
  }
}
