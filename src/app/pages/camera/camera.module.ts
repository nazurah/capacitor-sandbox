import { NgModule } from '@angular/core';
import { CameraPageRoutingModule } from './camera-routing.module';
import { CameraPage } from './camera.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [SharedModule, CameraPageRoutingModule],
  declarations: [CameraPage],
})
export class CameraPageModule {}
