import { Component, OnInit } from '@angular/core';

import { Plugins } from '@capacitor/core';
import { NativeService } from 'src/app/shared/services/native.service';

const { NativeBiometric } = Plugins;

@Component({
  selector: 'app-biometric',
  templateUrl: './biometric.page.html',
  styleUrls: ['./biometric.page.scss'],
})
export class BiometricPage implements OnInit {
  availability;
  constructor(private nativeService: NativeService) {}

  ngOnInit() {}

  async getBiometricAvailability() {
    if (!this.nativeService.isNative()) return;
    try {
      this.availability = await NativeBiometric.isAvailable();
      alert(JSON.stringify(this.availability));
    } catch (error) {
      alert(JSON.stringify(error));
    }
  }

  async authenticate() {
    if (this.availability.isAvailable) {
      try {
        let result = await NativeBiometric.verifyIdentity({
          reason: 'For easy log in',
          title: 'Log in',
          subtitle: 'Maybe add subtitle here?',
          description: 'Maybe a description too?',
        });

        alert(JSON.stringify(result));
      } catch (error) {
        alert(JSON.stringify(error));
      }
    }
  }
}
