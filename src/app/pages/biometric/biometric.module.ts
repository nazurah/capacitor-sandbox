import { NgModule } from '@angular/core';
import { BiometricPageRoutingModule } from './biometric-routing.module';
import { BiometricPage } from './biometric.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [SharedModule, BiometricPageRoutingModule],
  declarations: [BiometricPage],
})
export class BiometricPageModule {}
