import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  navs = [
    {
      name: 'Camera',
      path: 'camera',
      icon: 'camera',
    },
    {
      name: 'Camera Preview',
      path: 'camera-preview',
      icon: 'aperture-outline',
    },
    {
      name: 'QR Scanner',
      path: 'scanner',
      icon: 'scan',
    },
    {
      name: 'Biometric',
      path: 'biometric',
      icon: 'finger-print',
    },
  ];

  constructor(private navCtrl: NavController) {}

  async ngOnInit() {}
}
