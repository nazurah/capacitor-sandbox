import { Component, OnInit } from '@angular/core';
import { Plugins } from '@capacitor/core';

const { CameraPreview } = Plugins;
import { CameraPreviewOptions } from '@capacitor-community/camera-preview';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-camera-preview',
  templateUrl: './camera-preview.page.html',
  styleUrls: ['./camera-preview.page.scss'],
})
export class CameraPreviewPage implements OnInit {
  form: FormGroup;
  base64PictureData: string;

  cameraActive: boolean;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.form = this.formBuilder.group({
      position: ['rear', Validators.required],
      width: window.screen.width,
      height: window.screen.height - 132,
      x: 0,
      y: 56,
      toBack: false,
      paddingBottom: 0,
      rotateWhenOrientationChanged: true,
      storeToFile: false,
      disableExifHeaderStripping: true,
      disableAudio: false,
      lockAndroidOrientation: false,
    });
  }

  async start() {
    if (this.form.valid) {
      this.cameraActive = true;

      const cameraPreviewOptions: CameraPreviewOptions = this.form.value;
      await CameraPreview.start(cameraPreviewOptions);
    }
  }

  async capture() {
    const cameraPreviewOptions: CameraPreviewOptions = this.form.value;
    const result = await CameraPreview.capture(cameraPreviewOptions);
    this.base64PictureData = 'data:image/png;base64,' + result.value;

    this.cameraActive = false;

    await this.stop();
  }

  async stop() {
    this.cameraActive = false;
    await CameraPreview.stop();
  }

  flip() {
    CameraPreview.flip();
  }

  ionViewDidLeave() {
    this.cameraActive = false;
  }
}
