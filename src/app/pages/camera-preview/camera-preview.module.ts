import { NgModule } from '@angular/core';
import { CameraPreviewPageRoutingModule } from './camera-preview-routing.module';

import { CameraPreviewPage } from './camera-preview.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [SharedModule, CameraPreviewPageRoutingModule],
  declarations: [CameraPreviewPage],
})
export class CameraPreviewPageModule {}
