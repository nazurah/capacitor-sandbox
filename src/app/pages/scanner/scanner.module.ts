import { NgModule } from '@angular/core';
import { ScannerPageRoutingModule } from './scanner-routing.module';
import { ScannerPage } from './scanner.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [SharedModule, ScannerPageRoutingModule],
  declarations: [ScannerPage],
})
export class ScannerPageModule {}
