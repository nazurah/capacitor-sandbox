import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { AlertController } from '@ionic/angular';

const { BarcodeScanner } = Plugins;
@Component({
  selector: 'app-scanner',
  templateUrl: './scanner.page.html',
  styleUrls: ['./scanner.page.scss'],
})
export class ScannerPage implements OnInit, AfterViewInit, OnDestroy {
  scannerActive: boolean;
  result: string;

  constructor(private alertCtrl: AlertController) {}

  ngOnInit() {}

  ngAfterViewInit() {
    BarcodeScanner.prepare();
  }

  ngOnDestroy() {
    this.scannerActive = false;
  }

  async startScanner() {
    const allowed = await this.getScannerPermission();

    if (allowed) {
      const result = await BarcodeScanner.startScan();

      if (result.hasContent) {
        this.scannerActive = false;
        this.result = result.content;
      }
    }
  }

  async getScannerPermission() {
    return new Promise(async (resolve) => {
      const status = await BarcodeScanner.checkPermission({ force: true });

      if (status.granted) {
        resolve(true);
        this.scannerActive = true;
      } else if (status.denied) {
        const alert = await this.alertCtrl.create({
          header: 'No Permission',
          message: 'Please allow camera access in your settings',
          buttons: [
            {
              text: 'No',
              role: 'cancel',
            },
            {
              text: 'Open Settings',
              handler: () => {
                resolve(false);
                BarcodeScanner.openAppSettings();
              },
            },
          ],
        });

        await alert.present();
      } else {
        resolve(false);
      }
    });
  }

  async stopScanner() {
    BarcodeScanner.stopScan();
    this.scannerActive = false;
  }
}
