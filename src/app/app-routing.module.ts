import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'scanner',
    loadChildren: () =>
      import('./pages/scanner/scanner.module').then((m) => m.ScannerPageModule),
  },
  {
    path: 'camera',
    loadChildren: () =>
      import('./pages/camera/camera.module').then((m) => m.CameraPageModule),
  },
  {
    path: 'biometric',
    loadChildren: () =>
      import('./pages/biometric/biometric.module').then(
        (m) => m.BiometricPageModule
      ),
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./pages/home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: 'camera-preview',
    loadChildren: () =>
      import('./pages/camera-preview/camera-preview.module').then(
        (m) => m.CameraPreviewPageModule
      ),
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
